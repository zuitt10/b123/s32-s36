const Course = require('../models/Course')

module.exports.addCourse = (req,res)=>{

	console.log(req.body);
		Course.findOne({name: req.body.name},(err,result)=>{


			if(result !== null && result.name === req.body.name){

				return res.send("Course is already registered");

			} else {

					let newCourse = new Course({

						name: req.body.name,
						description: req.body.description,
						price: req.body.price

					})

					newCourse.save((saveErr,savedCourse)=>{

						console.log(savedCourse);

						if(saveErr){

							return console.error(saveErr);

						} else {

							return res.send(savedCourse);

						}

					})


			}

		})

}

module.exports.getAllCourseController = (req,res)=>{

	Course.find({}).then(result =>  res.send(result)).catch(err => res.send(err))
}

module.exports.getActiveCoursesController = (req,res) => {

Course.find({isActive: true}).then(result =>  res.send(result)).catch(err => res.send(err))
	}



module.exports.getSingleCourseController = (req,res) =>{
	// Mongoose has a querry called findById() which works like find({_id:"id"})
	console.log(req.params.id)//the id passed from your url.
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateCourse = (req,res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
}

module.exports.archiveCourse = (req,res) => {

	
		let updates = {isActive: false}
	

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
}

module.exports.activateCourse = (req,res) => {

	
		let updates = {isActive: true}
	

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
}

module.exports.getEnrolleesController = (req,res) => {

	Course.findById(req.params.id).then(result =>  res.send(result.enrollees)).catch(err => res.send(err))

}