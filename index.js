const express = require('express');
const mongoose = require ('mongoose');
// cors is a package used a middleware so that we can approve the use of our routes and controllers by other applications such our front-end applications
const cors = require('cors')

const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://jericb123:Ravenger1@cluster0.9tqks.mongodb.net/Booking?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology: true
}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () =>console.log("Connected to MongoDb"));

app.use(cors());
app.use(express.json());

const courseRoutes = require('./routes/courseRoutes');
console.log(courseRoutes);
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
console.log(userRoutes);
app.use('/users',userRoutes);

app.listen(port,()=>console.log(`server running at ${port}`))