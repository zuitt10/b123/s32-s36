const mongoose = require("mongoose");

	const userSchema = new mongoose.Schema({

			firstName: {
				type: String,
				required: [true, "First name is Required"]
			},
			lastName: {
				type: String,
				required: [true, "Last name is Required"]
			},
			mobileNo: {
				type: String,
				required: [true, "Mobile Number is Required"]
			},
			password: {
				type: String,
				required: [true, "Password is Required"]
			},
			email: {
				type: String,
				required: [true, "Mobile Number is Required"]
			},
			isAdmin: {
				type: Boolean,
				default: false
			},
			enrollments: [
			{
				courseId: {
					type: String,
					required: [true, "Course ID is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
		

}) //end of model

	module.exports = mongoose.model("User", userSchema)