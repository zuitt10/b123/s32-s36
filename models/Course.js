const mongoose = require("mongoose");

	const courseSchema = new mongoose.Schema({

		name: {
			type: String,
			required: [true, "Name is Required."]
		},
		description: {
			type: String,
			required: [true, "description is Required."]
		},
		price: {
			type: Number,
			required: [true, "price is Required."]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn:{
			type: Date,
			default: new Date()
		},
		/*
			In MOngoDB, we can have 2 way embedding for models with many to many relationship.
			In MondoDB, to impletement 2 way embedding we add subdocument array n both models.
			Each subdocument array also has a schema for its subdocuments.

		*/
		enrollees: [
			{
				userId:{
					type:String,
					required:[true, "User Id is required."]
				},
				enrolled: {
					type:String,
					default: new Date()
				},
				status:{
					type: String,
					default: "Enrolled"
				}
			}
		]

	}) //end of model

	module.exports = mongoose.model("Course", courseSchema)