const express = require('express');
const router = express.Router();
const courseControllers = require('../controllers/courseControllers');
const {addCourse,
	   getAllCourseController,
	   getActiveCoursesController,
	   getSingleCourseController,
	   updateCourse,
	   archiveCourse,
	   activateCourse,
	   getEnrolleesController
	  } = courseControllers;
const auth = require ('../auth')
const {verify,verifyAdmin} = auth;

// add course(admin only)
router.post('/',verify,verifyAdmin,addCourse);
// get all courses if the user is an admin
router.get('/',verify,verifyAdmin,getAllCourseController);
// get only the active courses
router.get('/getActiveCourses',getActiveCoursesController);
// get single course by Id
router.get('/getSingleCourse/:id',getSingleCourseController);
// update a single document(admin only)
router.put('/:id',verify,verifyAdmin,updateCourse)
//archive a docoument(admin only)
router.put('/archive/:id',verify,verifyAdmin,archiveCourse)
//activate a document(adminonly)
router.put('/activate/:id',verify,verifyAdmin,activateCourse)
router.get('/getEnrollees/:id',verify,verifyAdmin,getEnrolleesController)
module.exports = router;
