const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');
const {addUser,
	   loginUser,
	   getSingleUserController,
	   updateProfile,
	   enroll,
	   checkEmailExists,
	   getEnrollmentsController
	  } = userControllers;
const auth = require('../auth')
// verify is a method from our auth module which will let us verify if the jwt is legitimate and let us decode the payload.
const {verify} = auth;
// add user
router.post('/',addUser);
// login user
router.post('/login',loginUser);

//getSingleUser
// route metods, much like middlewares, give access to req,res and next objects for the functions that are  included in them.
// In fact, in Expressjs, we can have multiple layers of middleware to do tasks before letting another function perform another task.
router.get('/getUserDetails',verify,getSingleUserController);
//update the logged in user profile
router.put('/updateProfile',verify,updateProfile);
//enroll route
router.post('/enroll',verify,enroll);

// check email eixst route
router.post('/checkEmailExists',checkEmailExists)
// get enrollment of logged user
router.get('/getEnrollments',verify,getEnrollmentsController)
module.exports = router;